import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0
import QtQuick.Controls.Universal 2.0
import QtQuick.Dialogs 1.2
import HackEdit.Editor 0.1
import HackEdit.Common.Icons 0.1


ApplicationWindow {
    id: window
    visible: true
    width: 1280
    height: 720
    title: "HackEdit Editor - New document"

    Material.theme: settings.theme
    Material.primary: Material.theme == Material.Dark ? Qt.lighter(Material.background, 1.1) : Material.BlueGrey
    Material.accent: Material.theme == Material.Dark ? Material.Blue : Material.BlueGrey

    header: EditToolBar { id: toolBar }
    SettingsDrawer { id: settingsDrawer }

    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        folder: shortcuts.home
        selectExisting: true
        onAccepted: {
            console.log("You chose: " + fileDialog.fileUrl)
            openDocument(fileDialog.fileUrl);
        }
    }

    FileDialog {
        id: saveFileDialog
        title: "Please destination a file"
        folder: shortcuts.home
        selectExisting: false
        onAccepted: {
            console.log("You chose: " + saveFileDialog.fileUrl)
            saveNewDocument(saveFileDialog.fileUrl);
        }
    }

    function newDocument() {
        editor.document.text = "New document"
        editor.file.path = ""
        updateWindowTitle()
    }

    function openDocument(path) {
        console.log("Opening existing document " + path);
        editor.file.path = path;
    }

    function saveDocument() {
        if(editor.file.path) {
            editor.document.text = editor.text // remove once we got rid of TextArea and document has a line model that is modifiable
            editor.file.save();
        }
        else
            saveFileDialog.open();
    }

    function saveNewDocument(path) {
        editor.document.text = editor.text // remove once we got rid of TextArea and document has a line model that is modifiable
        editor.file.save(path);
        updateWindowTitle()
    }

    function undo() {
        console.log("Undo");
    }

    function redo() {
        console.log("Redo");
    }

    CodeEditor {
        id: editor
        anchors.fill: parent
    }

    Connections {
        target: editor.file
        onPathChanged: updateWindowTitle()
    }

    function updateWindowTitle() {
        var fileName = editor.file.fileName();
        if(!fileName)
            fileName = "New document";
        window.title = "HackEdit Editor - " + fileName;
    }

    footer: ToolBar { }
}
