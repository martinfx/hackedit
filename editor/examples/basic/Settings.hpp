#pragma once

#include <QObject>
#include <memory>

class QSettings;

class Settings: public QObject {
    Q_OBJECT
    Q_PROPERTY(QString theme READ theme WRITE setTheme NOTIFY themeChanged)
public:
    Settings(QObject* parent=nullptr);
    ~Settings();
    QString theme() const;

public slots:
    void setTheme(QString value);

signals:
    void themeChanged(QString);

private:
    std::unique_ptr<QSettings> _settings;
};
