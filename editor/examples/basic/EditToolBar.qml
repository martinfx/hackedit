import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0
import HackEdit.Common.Icons 0.1

ToolBar {
    id: toolBar
    property int iconSize: 24

    RowLayout {
        anchors.fill: parent
        ToolButton {
            text: MaterialIcons.icon_create
            font.family: MaterialIcons.family
            font.pixelSize: toolBar.iconSize
            implicitHeight: toolBar.implicitHeight
            implicitWidth: toolBar.implicitHeight

            onClicked: window.newDocument();
            Shortcut {
                sequence: StandardKey.New
                onActivated: window.newDocument();
            }
        }

        ToolButton {
            text: MaterialIcons.icon_folder_open
            font.family: MaterialIcons.family
            font.pixelSize: toolBar.iconSize
            implicitHeight: toolBar.implicitHeight
            implicitWidth: toolBar.implicitHeight

            onClicked: fileDialog.open();
            Shortcut {
                sequence: StandardKey.Open
                onActivated: fileDialog.open();
            }
        }
        ToolButton {
            text: MaterialIcons.icon_save
            font.family: MaterialIcons.family
            font.pixelSize: toolBar.iconSize
            implicitHeight: toolBar.implicitHeight
            implicitWidth: toolBar.implicitHeight

            onClicked: window.saveDocument()
            Shortcut {
                sequence: StandardKey.Save
                onActivated: window.saveDocument()
            }
        }

        // separator
        Item {
            Layout.preferredWidth: 7
            Layout.fillHeight: true
            Rectangle {
                anchors.centerIn: parent
                anchors.margins: 3
                height: toolBar.implicitHeight * 0.75
                width: 1
                color: Material.color(Material.Grey)
            }
        }
        ToolButton {
            text: MaterialIcons.icon_undo
            font.family: MaterialIcons.family
            font.pixelSize: toolBar.iconSize
            implicitHeight: toolBar.implicitHeight
            implicitWidth: toolBar.implicitHeight

            onClicked: window.undo()
            Shortcut {
                sequence: StandardKey.Undo
                onActivated: undo()
            }
        }
        ToolButton {
            text: MaterialIcons.icon_redo
            font.family: MaterialIcons.family
            font.pixelSize: toolBar.iconSize
            implicitHeight: toolBar.implicitHeight
            implicitWidth: toolBar.implicitHeight

            onClicked: window.redo()
            Shortcut {
                sequence: StandardKey.Redo
                onActivated: redo()
            }
        }
        Item { Layout.fillWidth: true }
        ToolButton {
            text: MaterialIcons.icon_settings
            font.family: MaterialIcons.family
            font.pixelSize: toolBar.iconSize

            implicitHeight: toolBar.implicitHeight
            implicitWidth: toolBar.implicitHeight

            onClicked: {
                settingsDrawer.open()
            }
        }
    }
}
