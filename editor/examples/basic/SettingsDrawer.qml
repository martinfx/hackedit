import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0
import HackEdit.Common.Icons 0.1

Drawer {
    id: settingsDrawer
    width: 480
    height: window.height

    Page {
        anchors.fill: parent
        header: ToolBar {
            Material.elevation: 0

            ToolButton {
                text: MaterialIcons.icon_arrow_back
                font.pixelSize: 24
                font.family: MaterialIcons.family

                onClicked: {
                    settingsDrawer.close()
                }
            }

            Label {
                text: "Settings"
                font.pixelSize: 25
                anchors.centerIn: parent
            }
        }

        Flickable {
            anchors.fill: parent
            contentHeight: contentItem.height + 2 * contentItem.anchors.margins
            flickableDirection: Flickable.AutoFlickIfNeeded
            boundsBehavior: Flickable.StopAtBounds
            clip: true
            
            ColumnLayout {
                id: contentItem
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: 9

                GroupBox {
                    title: "Appearance"        
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    
                    label: Label {
                        text: parent.title
                        font.pixelSize: 25
                    }
                    
                    background: Rectangle {
                        color: "transparent"
                        border.color: "transparent"
                    }
                    
                    ColumnLayout {
                        id: layout
                        anchors.fill: parent
                        
                        GridLayout {
                            Layout.fillWidth: true
                            columns: 2

                            Label {
                                id: lblTheme
                                text: "Theme: "
                            }
                            
                            ComboBox {
                                id: theme
                                Layout.fillWidth: true
                                model: ["Light", "Dark"]
                                property bool _completed: false
                                onCurrentTextChanged: {
                                      if(!_completed)
                                          return;
                                      settings.theme = currentText;
                                }
                                Component.onCompleted: {
                                    _completed = true;
                                    currentIndex = theme.find(settings.theme);
                                }
                            }
                        }                        
                    }
                }
            
                GroupBox {
                    title: "Behaviour"
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    
                    label: Label {
                        text: parent.title
                        font.pixelSize: 25
                    }
                    
                    background: Rectangle {
                        color: "transparent"
                        border.color: "transparent"
                    }
                }
            }
            
            ScrollBar.vertical: ScrollBar {
                active: size < 1
            }
        }
    }
}
