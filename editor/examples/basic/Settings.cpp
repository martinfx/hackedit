#include "Settings.hpp"
#include <QSettings>

using namespace std;

Settings::Settings(QObject *parent) :
        QObject(parent), _settings(make_unique<QSettings>("org.hackedit.examples", "Editor")) {
}

Settings::~Settings() = default;

QString Settings::theme() const {
    return _settings->value("theme", "Dark").toString();
}

void Settings::setTheme(QString value) {
    if(value == theme())
        return;

    _settings->setValue("theme", value);
    emit themeChanged(value);
}
