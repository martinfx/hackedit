import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Dialogs 1.1

import HackEdit.Editor 0.1

TextArea {
    id: root
    property var document: TextDocument { text: "New document" }
    text: document.text
    font.family: "Monospace"

    MessageDialog {
        id: errorDialog
        icon: StandardIcon.Critical
    }

    property var file: FileLoader {
        document: root.document
        encoding: encodings.systemEncoding()
        eolConvention: EndOfLine.Linux
        onError: {
            errorDialog.title = lastError.type;
            errorDialog.text = lastError.message;
            errorDialog.visible = true;
        }
    }

    BusyIndicator {
        anchors.centerIn: parent
        visible: file.isLoading || file.isSaving
    }
}
