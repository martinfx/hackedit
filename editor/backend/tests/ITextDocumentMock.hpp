#include <gmock/gmock.h>

#include <hackedit/editor/backend/ITextDocument.hpp>

class ITextDocumentMock: public HackEdit::Editor::Backend::ITextDocument {
public:
    MOCK_CONST_METHOD0(text, QString ());
    MOCK_METHOD1(setText, void (QString text));
};
