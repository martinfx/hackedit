#include <hackedit/editor/backend/TextDocument.hpp>
#include <QSignalSpy>
#include <gtest/gtest.h>

USE_HACKEDIT_NAMESPACE2(Editor, Backend)

class TextDocumentTests: public testing::Test {

};

TEST_F(TextDocumentTests, defaultCtor_test)
{
    TextDocument doc;
    EXPECT_EQ(doc.text(), "");
}

TEST_F(TextDocumentTests, setText_test)
{
    TextDocument doc;
    QSignalSpy spy(&doc, SIGNAL(textChanged(const QString&)));
    EXPECT_EQ(doc.text(), "");
    doc.setText("some text");
    EXPECT_EQ(spy.count(), 1);
    EXPECT_EQ(doc.text(), "some text");
    doc.setText("some text");
    EXPECT_EQ(spy.count(), 1);
}
