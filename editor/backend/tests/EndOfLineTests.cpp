#include <gtest/gtest.h>
#include <hackedit/editor/backend/EndOfLine.hpp>

USE_HACKEDIT_NAMESPACE2(Editor, Backend)

TEST(EndOfLineTests, separator_test) {
    EXPECT_EQ(EndOfLine::separator(EndOfLine::Linux), "\n");
    EXPECT_EQ(EndOfLine::separator(EndOfLine::Mac), "\r");
    EXPECT_EQ(EndOfLine::separator(EndOfLine::Windows), "\r\n");
    EXPECT_NE(EndOfLine::separator(EndOfLine::System), "");
}

TEST(EndOfLineTest, autoDetect_linuxEol_test) {
    QString contents = "blabla\nbla";
    EXPECT_EQ(EndOfLine::autoDetect(contents), "\n");
}

TEST(EndOfLineTest, autoDetect_macEol_test) {
    QString contents = "blabla\rbla";
    EXPECT_EQ(EndOfLine::autoDetect(contents), "\r");
}

TEST(EndOfLineTest, autoDetect_windowsEol_test) {
    QString contents = "blabla\r\nbla";
    EXPECT_EQ(EndOfLine::autoDetect(contents), "\r\n");
}

TEST(EndOfLineTest, autoDetect_mixedEol_test) {
    QString contents = "bla\nbla\rbla\r\nbla";
    EXPECT_EQ(EndOfLine::autoDetect(contents), "\r\n");
    contents = "bla\nbla\rbla";
    EXPECT_EQ(EndOfLine::autoDetect(contents), "\r");
}

TEST(EndOfLineTest, autoDetect_emptyStringReturnsEolConventionSerparator_test) {
    QString contents = "";
    EXPECT_EQ(EndOfLine::autoDetect(contents, EndOfLine::Mac), "\r");
}
