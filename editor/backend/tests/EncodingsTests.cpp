#include <gtest/gtest.h>
#include <hackedit/editor/backend/Encodings.hpp>
#include <random>
#include <QtTest/QSignalSpy>

USE_HACKEDIT_NAMESPACE2(Editor, Backend)

TEST(EncodingsTests, availableEncodings_test) {
    Encodings encodings;
    EXPECT_NE(encodings.availableEncodings().count(), 0);
}

TEST(EncodingsTests, systemEncoding_test) {
    Encodings encodings;
    EXPECT_NE(encodings.systemEncoding(), "");
}
