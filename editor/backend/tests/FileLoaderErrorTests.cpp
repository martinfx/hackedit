#include <gtest/gtest.h>
#include <hackedit/editor/backend/FileLoaderError.hpp>

USE_HACKEDIT_NAMESPACE2(Editor, Backend)

TEST(FileLoaderErrorTests, defaultCtor_test)
{
    FileLoaderError error;
    EXPECT_EQ(error.message(), "");
    EXPECT_EQ(error.type(), "No error");
    EXPECT_EQ(error.errorType(), FileLoaderError::NoError);
}

TEST(FileLoaderErrorTests, ctor_OpenFileFailedError_test)
{
    FileLoaderError error("An unexepected error occured...", FileLoaderError::OpenFileFailed);
    EXPECT_EQ(error.message(), "An unexepected error occured...");
    EXPECT_EQ(error.type(), "Open file failed");
    EXPECT_EQ(error.errorType(), FileLoaderError::OpenFileFailed);
}

TEST(FileLoaderErrorTests, ctor_SaveFileFailed_test)
{
    FileLoaderError error("An unexepected error occured...", FileLoaderError::SaveFileFailed);
    EXPECT_EQ(error.message(), "An unexepected error occured...");
    EXPECT_EQ(error.type(), "Save file failed");
    EXPECT_EQ(error.errorType(), FileLoaderError::SaveFileFailed);
}

TEST(FileLoaderErrorTests, ctor_DecodingError_test)
{
    FileLoaderError error("An unexepected error occured...", FileLoaderError::DecodingError);
    EXPECT_EQ(error.message(), "An unexepected error occured...");
    EXPECT_EQ(error.type(), "Decoding error");
    EXPECT_EQ(error.errorType(), FileLoaderError::DecodingError);
}

TEST(FileLoaderErrorTests, ctor_EncodingError_test)
{
    FileLoaderError error("An unexepected error occured...", FileLoaderError::EncodingError);
    EXPECT_EQ(error.message(), "An unexepected error occured...");
    EXPECT_EQ(error.type(), "Encoding error");
    EXPECT_EQ(error.errorType(), FileLoaderError::EncodingError);
}
