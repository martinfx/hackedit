#pragma once

#include <QObject>
#include <QQmlParserStatus>
#include <QtConcurrent>
#include <hackedit/common/HackEditNamespace.hpp>
#include <hackedit/common/logging/LoggingManager.hpp>
#include <hackedit/editor/backend/EndOfLine.hpp>
#include <hackedit/editor/backend/Exports.hpp>
#include <hackedit/editor/backend/FileLoaderError.hpp>
#include <hackedit/editor/backend/TextDocument.hpp>

BEGIN_HACKEDIT_NAMESPACE2(Editor, Backend)

/**
 * @brief The FileLoader allow to load and save file from QML and set the text property of the linked text document.
 */
class BACKEND_EXPORT FileLoader: public QObject, public QQmlParserStatus {
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    typedef std::unique_ptr<FileLoaderError> FileLoaderErrorPtr;

    /**
     * @brief The path to load/save
     * @accessors path(), setPath()
     */
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)

    /**
     * @brief The codec to use to decode and encode file (e.g. "UTF-8").
     * @accessors encoding(), setEncoding()
     */
    Q_PROPERTY(QString encoding READ encoding WRITE setEncoding NOTIFY encodingChanged)

    /**
     * @brief If true, the loader will try to detect the EOL character of the file during load, that character will be
     * used for saving the file.
     * @accessors autoDetectEol(), setAutoDetectEol()
     */
    Q_PROPERTY(bool autoDetectEol READ autoDetectEol WRITE setAutoDetectEol NOTIFY autoDetectEolChanged)

    /**
     * @brief The EOL convention to use to save a file. Ignored if autoDetectEOL is true and the auto detection
     * succeeded.
     * @accessors eolConvention(), setEolConcention()
     */
    Q_PROPERTY(int eolConvention READ eolConvention WRITE setEolConvention NOTIFY eolConventionChanged)

    /**
     * @brief The target text document instance.
     *
     * When loading a file, the document's text property will be set to the file content.
     * When saving a file, the file content is taken from the document's text property.
     *
     * @accessors document(), setDocument()
     */
    Q_PROPERTY(ITextDocument* document READ document WRITE setDocument NOTIFY documentChanged)

    /**
     * @brief The last error that occured. Null if no error occured.
     */
    Q_PROPERTY(FileLoaderError* lastError READ lastError CONSTANT)

    /**
     * @brief Specify if the loader is busy loading a file.
     *
     * Use this to show a busy indicator. This property is read-only and can be tracked using the isLoadingChanged
     * signal.
     *
     * @accessors isLoading
     */
    Q_PROPERTY(bool isLoading READ isLoading WRITE setIsLoading NOTIFY isLoadingChanged)

    /**
     * @brief Specify if the loader is busy saving a file.
     *
     * Use this to show a busy indicator. This property is read-only and can be tracked using the isSavingChanged
     * signal.
     */
    Q_PROPERTY(bool isSaving READ isSaving WRITE setIsSaving NOTIFY isSavingChanged)

public:
    FileLoader(QObject* parent=nullptr);

    void classBegin() override;
    void componentComplete() override;

    const QString& path() const;
    QString encoding() const;
    bool autoDetectEol() const;
    int eolConvention() const;
    ITextDocument* document() const;
    FileLoaderError* lastError() const;
    bool isLoading() const;
    bool isSaving() const;
    QString getEolString() const;

    Q_INVOKABLE void reload();
    Q_INVOKABLE void save();
    Q_INVOKABLE void save(const QString& path);
    Q_INVOKABLE QString fileName() const;


public slots:
    void setPath(const QString& path);
    void setEncoding(const QString& encoding);
    void setAutoDetectEol(bool autoDetectEol);
    void setEolConvention(int eol);
    void setDocument(ITextDocument* document);

private slots:
    void onAsyncLoadFinished();
    void onAsyncSaveFinished();
    void setIsLoading(bool isLoading);
    void setIsSaving(bool isSaving);

signals:
    void pathChanged(QString path);
    void encodingChanged(QString encoding);
    void autoDetectEolChanged();
    void eolConventionChanged();
    void documentChanged();
    void isLoadingChanged();
    void isSavingChanged();
    void loadFinished();
    void saveFinished();
    void error();

private:
    void loadFile();
    QString loadFileAsync(const QString& path, const QString& encoding);
    void saveFileAsync(const QString& text, const QString& path, const QString& encoding);
    void setError(FileLoaderErrorPtr newError);
    void resetError();
    void detectEol(const QString &contents);
    QString cleanPath(const QString& path);

    Common::Logging::ILoggerPtr _logger;
    bool _complete;
    bool _isLoading, _isSaving;
    QString _path;
    QString _encoding;
    bool _autoDetectEol;
    EndOfLine::Convention _eol;
    ITextDocument* _document;
    FileLoaderErrorPtr _lastError;
    QString _errorMessage;
    QString _detectedEol;

    QFutureWatcher<QString>* _loadFutureWatcher;
    QFuture<QString> _loadFuture;
    QFutureWatcher<void>* _saveFutureWatcher;
    QFuture<void> _saveFuture;
};

END_HACKEDIT_NAMESPACE2
