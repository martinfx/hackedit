#pragma once

#include <hackedit/common/HackEditNamespace.hpp>
#include <hackedit/editor/backend/Exports.hpp>
#include <QtCore/QObject>

BEGIN_HACKEDIT_NAMESPACE2(Editor, Backend)

class BACKEND_EXPORT EndOfLine: public QObject
{
    Q_OBJECT
public:
    enum Convention {
        System,
        Linux,
        Mac,
        Windows
    };
    Q_ENUM(Convention)

    static QString separator(Convention convention) {
        QString retVal;
        switch (convention) {
            case System:
                #if defined(Q_OS_WIN32)
                    retVal = separator(Windows);
                #elif defined(Q_OS_MAC)
                    retVal = separator(Mac);
                #else
                    retVal = separator(Linux);
                #endif
                break;
            case Linux:
                retVal = "\n";
                break;
            case Mac:
                retVal = "\r";
                break;
            case Windows:
                retVal = "\r\n";
                break;
        }
        return retVal;
    }

    static QString autoDetect(const QString& string, Convention defaultConvention=System) {
        QString retVal = separator(defaultConvention);
        Convention toTest[] = { Windows, Mac, Linux };
        for(auto convention: toTest) {
            if (string.contains(separator(convention))) {
                retVal = separator(convention);
                break;
            }
        }
        return retVal;
    }
};

END_HACKEDIT_NAMESPACE2
