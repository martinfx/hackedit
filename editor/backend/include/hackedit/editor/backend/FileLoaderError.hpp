#pragma once

#include <hackedit/common/HackEditNamespace.hpp>
#include <hackedit/editor/backend/Exports.hpp>
#include <QObject>

BEGIN_HACKEDIT_NAMESPACE2(Editor, Backend)

class BACKEND_EXPORT FileLoaderError: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString message READ message CONSTANT)
    Q_PROPERTY(QString type READ type CONSTANT)
public:
    enum ErrorType {
        NoError,
        OpenFileFailed,
        SaveFileFailed,
        DecodingError,
        EncodingError,
    };
    Q_ENUMS(ErrorStatus)

    FileLoaderError(const QString& message="", ErrorType type=NoError);
    QString message() const;
    QString type() const;

    Q_INVOKABLE ErrorType errorType() const;

private:
    QString _message;
    ErrorType _type;
};

END_HACKEDIT_NAMESPACE2