#pragma once

#include <hackedit/common/HackEditNamespace.hpp>
#include <hackedit/editor/backend/Exports.hpp>
#include <QObject>
#include <QString>

BEGIN_HACKEDIT_NAMESPACE2(Editor, Backend)

/**
 * Defines the TextDocument interface. This class exists mainly to be able to mock the text document for unit tests.
 */
class BACKEND_EXPORT ITextDocument: public QObject {
    Q_OBJECT
public:
    ITextDocument(QObject* parent=nullptr);
    virtual ~ITextDocument();

    virtual QString text() const = 0;
    virtual void setText(QString text) = 0;
};

END_HACKEDIT_NAMESPACE2
