#pragma once

#include <QObject>
#include <hackedit/common/HackEditNamespace.hpp>
#include <hackedit/common/logging/LoggingManager.hpp>
#include <hackedit/editor/backend/Exports.hpp>
#include <hackedit/editor/backend/ITextDocument.hpp>
#include <memory>

BEGIN_HACKEDIT_NAMESPACE2(Editor, Backend)

/**
 * The TextDocument class holds the formatted text for use with the QML CodeEditor.
 * control.
 */
class BACKEND_EXPORT TextDocument: public ITextDocument {
    Q_OBJECT
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
public:
    TextDocument(QObject* parent= nullptr);

    /**
     * Gets the raw text (including modifications).
     *
     * @note This overload always use '\n' for line endings.
     *
     * @return the raw text
     */
    QString text() const override;

public slots:
    /**
     * Sets the raw text.
     * @param text
     */
    void setText(QString text) override;

signals:
    void textChanged(QString text);

private:
    QString _text; // todo replace with a LineModel.
    Common::Logging::ILoggerPtr _logger;
};

END_HACKEDIT_NAMESPACE2
