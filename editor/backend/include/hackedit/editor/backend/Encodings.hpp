#pragma once

#include <memory>
#include <QObject>
#include <hackedit/common/HackEditNamespace.hpp>
#include <hackedit/editor/backend/Exports.hpp>
#include <hackedit/common/logging/LoggingManager.hpp>

BEGIN_HACKEDIT_NAMESPACE2(Editor, Backend)

class BACKEND_EXPORT Encodings: public QObject
{
    Q_OBJECT
public:
    Encodings(QObject* parent= nullptr);

    Q_INVOKABLE QStringList availableEncodings();
    Q_INVOKABLE QString systemEncoding();

private:
    Common::Logging::ILoggerPtr _logger;
};

END_HACKEDIT_NAMESPACE2