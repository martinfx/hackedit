#include "ITextDocument.hpp"

USE_HACKEDIT_NAMESPACE2(Editor, Backend)

ITextDocument::ITextDocument(QObject *parent) : QObject(parent) {}

ITextDocument::~ITextDocument() = default;
