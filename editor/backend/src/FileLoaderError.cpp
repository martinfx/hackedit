#include "FileLoaderError.hpp"

USE_HACKEDIT_NAMESPACE2(Editor, Backend)

FileLoaderError::FileLoaderError(const QString &message, FileLoaderError::ErrorType type) :
        _message(message), _type(type) {

}

QString FileLoaderError::message() const {
    return _message;
}

FileLoaderError::ErrorType FileLoaderError::errorType() const {
    return _type;
}

QString FileLoaderError::type() const {
    QString retVal;
    switch (_type) {
        case OpenFileFailed:
            retVal = tr("Open file failed");
            break;
        case DecodingError:
            retVal = tr("Decoding error");
            break;
        case EncodingError:
            retVal = tr("Encoding error");
            break;
        case SaveFileFailed:
            retVal = tr("Save file failed");
            break;
        case NoError:
            retVal = tr("No error");
            break;
    }
    return retVal;
}
