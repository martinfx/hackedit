#include "Encodings.hpp"
#include <QTextCodec>

USE_HACKEDIT_NAMESPACE2(Common, Logging)
USE_HACKEDIT_NAMESPACE2(Editor, Backend)

Encodings::Encodings(QObject* parent) :
        QObject(parent), _logger(LoggingManager::logger("Encodings")) {
}

QStringList Encodings::availableEncodings() {
    QStringList codecs;
    for(QByteArray codec: QTextCodec::availableCodecs()) {
        codecs.append(QString::fromUtf8(codec));
    }
    return codecs;
}

QString Encodings::systemEncoding() {
    return QString::fromUtf8(QTextCodec::codecForLocale()->name());
}
