#include "FileLoader.hpp"
#include "ITextDocument.hpp"
#include <QtCore/QUrl>
#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QtCore/QTextCodec>
#include <QtConcurrent/QtConcurrent>
#include "Encodings.hpp"
#include <memory>

USE_HACKEDIT_NAMESPACE2(Editor, Backend)
USE_HACKEDIT_NAMESPACE2(Common, Logging)
using namespace std;

FileLoader::FileLoader(QObject* parent) :
        QObject(parent),
        _logger(LoggingManager::logger("HackEdit::Editor::Backend::FileLoader")),
        _complete(false),
        _isLoading(false),
        _isSaving(false),
        _encoding(Encodings().systemEncoding()),
        _autoDetectEol(true),
        _eol(EndOfLine::System),
        _document(nullptr),
        _loadFutureWatcher(new QFutureWatcher<QString>(this)),
        _saveFutureWatcher(new QFutureWatcher<void>(this)) {
    connect(_loadFutureWatcher, &QFutureWatcher<QString>::finished, this, &FileLoader::onAsyncLoadFinished);
    connect(_saveFutureWatcher, &QFutureWatcher<void>::finished, this, &FileLoader::onAsyncSaveFinished);
}

void FileLoader::classBegin() { }

void FileLoader::componentComplete() {
    _complete = true;
    loadFile();
}

const QString& FileLoader::path() const {
    return _path;
}

QString FileLoader::encoding() const {
    return _encoding;
}

bool FileLoader::autoDetectEol() const {
    return _autoDetectEol;
}

int FileLoader::eolConvention() const {
    return _eol;
}

ITextDocument* FileLoader::document() const {
    return _document;
}

FileLoaderError* FileLoader::lastError() const {
    return _lastError.get();
}

bool FileLoader::isLoading() const {
    return _isLoading;
}

bool FileLoader::isSaving() const {
    return _isSaving;
}

QString FileLoader::getEolString() const {
    return !_detectedEol.isEmpty() ? _detectedEol : EndOfLine::separator(_eol);
}

void FileLoader::reload() {
    loadFile();
}

void FileLoader::save() {
    if(!_isLoading && !_isSaving) {
        setIsSaving(true);
        LOG_DEBUG(_logger, "saving file: " << _path);
        _saveFuture = QtConcurrent::run(this, &FileLoader::saveFileAsync,
                                        _document->text().replace("\n", getEolString()), _path, _encoding);
        _saveFutureWatcher->setFuture(_saveFuture);
    }
}

void FileLoader::save(const QString& path) {
    _path = cleanPath(path);
    save();
}

QString FileLoader::fileName() const {
    return QFileInfo(_path).fileName();
}

void FileLoader::setPath(const QString& path) {
    QString cleanedPath = cleanPath(path);
    if (_path == cleanedPath)
        return;

    _path = cleanedPath;
    emit pathChanged(cleanedPath);
    loadFile();
}

void FileLoader::setEncoding(const QString& encoding) {
    if (_encoding == encoding)
        return;

    _encoding = encoding;
    emit encodingChanged(encoding);
    loadFile();
}

void FileLoader::setAutoDetectEol(bool autoDetectEol) {
    if (_autoDetectEol == autoDetectEol)
        return;

    _autoDetectEol = autoDetectEol;
    emit autoDetectEolChanged();
}

void FileLoader::setEolConvention(int eol) {
    if(eol == _eol)
        return;

    _eol = EndOfLine::Convention(eol);
    emit eolConventionChanged();
}

void FileLoader::setDocument(ITextDocument* document) {
    if (_document == document)
        return;

    _document = document;
    emit documentChanged();
    loadFile();
}

void FileLoader::onAsyncLoadFinished() {
    LOG_DEBUG(_logger, "load finished");
    setIsLoading(false);
    if(lastError() == nullptr)
        _document->setText(_loadFutureWatcher->result());
    emit loadFinished();
}

void FileLoader::onAsyncSaveFinished() {
    LOG_DEBUG(_logger, "save finished");
    setIsSaving(false);
    emit saveFinished();
}

void FileLoader::setIsLoading(bool isLoading) {
    _isLoading = isLoading;
    isLoadingChanged();
}

void FileLoader::setIsSaving(bool isSaving) {
    _isSaving = isSaving;
    isSavingChanged();
}

void FileLoader::loadFile() {
    if (_complete && !_path.isEmpty() && _document != nullptr && !_isLoading && !_isSaving) {
        setIsLoading(true);
        LOG_DEBUG(_logger, "loading file: " << _path << ", encoding=" << _encoding);
        _loadFuture = QtConcurrent::run(this, &FileLoader::loadFileAsync, _path, _encoding);
        _loadFutureWatcher->setFuture(_loadFuture);
    }
}

QString FileLoader::loadFileAsync(const QString& path, const QString& encoding) {
    QFile file(path);

    // read file
    if (!file.open(QIODevice::ReadOnly)) {
        setError(make_unique<FileLoaderError>(
                "Failed to open file " + _path + "\n Error: " + file.errorString(),
                FileLoaderError::OpenFileFailed));
        return "";
    }
    auto data = file.readAll();

    // decode
    auto codec = QTextCodec::codecForName(encoding.toLocal8Bit());
    QTextCodec::ConverterState state;
    auto contents = codec->toUnicode(data.data(), data.size(), &state);
    if (state.invalidChars > 0) {
        setError(make_unique<FileLoaderError>(
                "Failed to decode file " + path + " with encoding " + encoding,
                FileLoaderError::DecodingError));
        return "";
    }
    detectEol(contents);
    resetError();

    return contents;
}

void FileLoader::saveFileAsync(const QString& text, const QString& path, const QString& encoding) {
    QFile file(path);
    if (!file.open(QIODevice::WriteOnly)) {
        setError(make_unique<FileLoaderError>(
                     "Failed to open file in WriteOnly mode: " + _path + "\n Error: " + file.errorString(),
                     FileLoaderError::OpenFileFailed));
        return;
    }
    // encode string
    auto codec = QTextCodec::codecForName(encoding.toLocal8Bit());
    auto contents = codec->fromUnicode(text);

    file.write(contents);

    resetError();
}

void FileLoader::setError(FileLoaderErrorPtr newError) {
    _lastError = std::move(newError);
    if (_lastError != nullptr) {
        LOG_ERROR(_logger, _lastError->message());
        emit error();
    }
}

void FileLoader::resetError() {
    setError(nullptr);
}

void FileLoader::detectEol(const QString &contents) {
    if(_autoDetectEol) {
        _detectedEol = EndOfLine::autoDetect(contents, EndOfLine::Convention(_eol));
        LOG_DEBUG(_logger, "auto detected eol: \"" << _detectedEol.replace("\n", "\\n").replace("\r", "\\r") << "\"");
    }
}

QString FileLoader::cleanPath(const QString& path)
{
    QString cleanedPath = path;
    if(cleanedPath.startsWith("file:///"))
        cleanedPath = QUrl(path).toLocalFile();
    cleanedPath = QDir::toNativeSeparators(cleanedPath);

    return cleanedPath;
}
