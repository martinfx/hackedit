include_directories(${CMAKE_SOURCE_DIR}/3rdparty/googletest)
set(GTEST_SRC ${CMAKE_SOURCE_DIR}/3rdparty/googletest/gmock-gtest-all.cc)
add_library(gtest STATIC ${GTEST_SRC})
enable_testing()
