# hackedit-core
[![build status](https://gitlab.com/HackEdit/hackedit-core/badges/master/build.svg)](https://gitlab.com/HackEdit/hackedit-core/commits/master)
[![coverage report](https://gitlab.com/HackEdit/hackedit-core/badges/master/coverage.svg)](https://gitlab.com/HackEdit/hackedit-core/commits/master)

The hackable and modern IDE, built with QML and Material design

This repository contains the core parts of the project:
 - the common libraries (logging,...)
 - the editor component (can be built separately from the application to be reused in other QtQuick/QtWidgets applications)
 - the application
 
# Requirements

## Supported platforms

- GNU/Linux
    - ArchLinux
    - Fedora >= 25
- Mac OSX
- Windows

## Dependencies

- **Qt** >= 5.7
- **CMake** >= 3.2
- **log4cplus** 1.2

# Compilation

Building hackedit requires a **C++14** compiler:
    - GCC >= 5.x
    - Clang >= 3.5
    - MSVC 2015 Update 2 

## GNU/Linux

```bash
mkdir build && cd build
cmake ..
make
```

## OSX

```bash
mkdir build && cd build
brew install qt5 cmake
cmake -DCMAKE_PREFIX_PATH=/usr/local/opt/qt5 ..
make
```

## Windows

```bash
mkdir build && cd build
cmake -G "Visual Studio 14 2015 Win64" -DCMAKE_BUILD_TYPE=Release .. 
cmake --build . --config Release
```

# Build options

You can specify the following options when calling cmake:

- ``-DCMAKE_BUILD_TYPE=``: *Debug* or *Release*
- ``-DBUILD_EXAMPLES``: *ON* or *OFF*
- ``-DBUILD_TESTS=``: *ON* or *OFF*
- ``-DBUILD_APPLICATION=``: *ON* or *OFF* (set this to OFF to build only the editor control and the common libs)
