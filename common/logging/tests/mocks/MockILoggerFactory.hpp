#pragma once

#include <gmock/gmock.h>
#include <hackedit/common/logging/ILoggerFactory.hpp>
#include <hackedit/common/logging/ILogger.hpp>

USE_HACKEDIT_NAMESPACE2(Common, Logging)

class MockILoggerFactory: public ILoggerFactory
{
public:
    virtual ~MockILoggerFactory() = default;
    MOCK_METHOD1(loggerProxy, ILogger*(const std::string&));
    std::unique_ptr<ILogger> logger(const std::string& name) {
        return std::unique_ptr<ILogger>(loggerProxy(name));
    }

};
