#include "mocks/MockILogger.hpp"
#include "mocks/MockILoggerFactory.hpp"
#include <hackedit/common/logging/LoggingManager.hpp>
#include <hackedit/common/Cpp14Support.hpp>
#include <gtest/gtest.h>

USE_HACKEDIT_NAMESPACE2(Common, Logging)
using testing::Return;
using testing::DefaultValue;

class TestLoggingManager: public testing::Test {

};

TEST_F(TestLoggingManager, LoggersAreNullsIfNoFactorySet_test)
{
    LoggingManager::initialize(nullptr);
    auto myLogger = LoggingManager::logger();
    EXPECT_EQ(myLogger, nullptr);
}

TEST_F(TestLoggingManager, LoggerFactory_logger_IsCalled_test)
{
    auto loggerFactory = std::make_unique<MockILoggerFactory>();
    EXPECT_CALL(*loggerFactory.get(), loggerProxy("test")).Times(1);

    LoggingManager::initialize(std::move(loggerFactory));
    LoggingManager::logger("test");
    LoggingManager::shutdown();
}
